<?php

namespace Drupal\organizer\Plugin\Group\Relation;

use Drupal\group\Plugin\Group\Relation\GroupRelationTypeInterface;
use Drupal\organizer\Entity\OrganizerType;
use Drupal\Component\Plugin\Derivative\DeriverBase;

class GroupOrganizerDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    assert($base_plugin_definition instanceof GroupRelationTypeInterface);
    $this->derivatives = [];

    foreach (OrganizerType::loadMultiple() as $name => $organizer_type) {
      $label = $organizer_type->label();

      $this->derivatives[$name] = clone $base_plugin_definition;
      $this->derivatives[$name]->set('entity_bundle', $name);
      $this->derivatives[$name]->set('label', t('Group organizer (@type)', ['@type' => $label]));
      $this->derivatives[$name]->set('description', t('Adds %type content to groups both publicly and privately.', ['%type' => $label]));
    }

    return $this->derivatives;
  }

}
