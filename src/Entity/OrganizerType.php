<?php declare(strict_types = 1);

namespace Drupal\organizer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Organizer type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "organizer_type",
 *   label = @Translation("Organizer type"),
 *   label_collection = @Translation("Organizer types"),
 *   label_singular = @Translation("organizer type"),
 *   label_plural = @Translation("organizers types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count organizers type",
 *     plural = "@count organizers types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\organizer\Form\OrganizerTypeForm",
 *       "edit" = "Drupal\organizer\Form\OrganizerTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\organizer\OrganizerTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "permissions" = "Drupal\user\Entity\EntityPermissionsRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer organizer types",
 *   bundle_of = "organizer",
 *   config_prefix = "organizer_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/organizer_types/add",
 *     "edit-form" = "/admin/structure/organizer_types/manage/{organizer_type}",
 *     "delete-form" = "/admin/structure/organizer_types/manage/{organizer_type}/delete",
 *     "entity-permissions-form" = "/admin/structure/organizer_types/manage/{organizer_type}/permissions",
 *     "collection" = "/admin/structure/organizer_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class OrganizerType extends ConfigEntityBundleBase {

  /**
   * The machine name of this organizer type.
   */
  protected string $id;

  /**
   * The human-readable name of the organizer type.
   */
  protected string $label;

}
