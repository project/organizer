<?php

namespace Drupal\organizer;

use Drupal\organizer\Entity\OrganizerType;
use Drupal\Core\Entity\BundlePermissionHandlerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class OrganizerPermissionsGenerator
 */
class OrganizerPermissionsGenerator {

  use BundlePermissionHandlerTrait;
  use StringTranslationTrait;

  /**
   * Returns an array of orgnaizer type permissions.
   *
   * @return array
   *   The organizer type permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function organizerTypePermissions() {
    return $this->generatePermissions(OrganizerType::loadMultiple(), [$this, 'buildPermissions']);
  }

  /**
   * Create the permissions desired for an individual entity type.
   *
   * @param OrganizerType $entity_type
   *
   * @return array
   */
  protected function buildPermissions(OrganizerType $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "create organizer $type_id content" => [
        'title' => $this->t('%type_name: Create new content', $type_params),
      ],
      "view own organizer $type_id content" => [
        'title' => $this->t('%type_name: View own content', $type_params),
        'description' => $this->t('Note that anonymous users with this permission are able to view any content created by any anonymous user.'),
      ],
      "view any organizer $type_id content" => [
        'title' => $this->t('%type_name: View any content', $type_params),
      ],      
      "edit own organizer $type_id content" => [
        'title' => $this->t('%type_name: Edit own content', $type_params),
        'description' => $this->t('Note that anonymous users with this permission are able to edit any content created by any anonymous user.'),
      ],
      "edit any organizer $type_id content" => [
        'title' => $this->t('%type_name: Edit any content', $type_params),
      ],
      "delete own organizer $type_id content" => [
        'title' => $this->t('%type_name: Delete own content', $type_params),
        'description' => $this->t('Note that anonymous users with this permission are able to delete any content created by any anonymous user.'),
      ],
      "delete any organizer $type_id content" => [
        'title' => $this->t('%type_name: Delete any content', $type_params),
      ],
      "view organizer $type_id revisions" => [
        'title' => $this->t('%type_name: View revisions', $type_params),
        'description' => t('To view a revision, you also need permission to view the content item.'),
      ],
      "revert organizer $type_id revisions" => [
        'title' => $this->t('%type_name: Revert revisions', $type_params),
        'description' => t('To revert a revision, you also need permission to edit the content item.'),
      ],
      "delete organizer $type_id revisions" => [
        'title' => $this->t('%type_name: Delete revisions', $type_params),
        'description' => $this->t('To delete a revision, you also need permission to delete the content item.'),
      ],
    ];
  }
}
