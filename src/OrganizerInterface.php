<?php declare(strict_types = 1);

namespace Drupal\organizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an organizer entity type.
 */
interface OrganizerInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
