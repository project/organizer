<?php declare(strict_types = 1);

namespace Drupal\organizer;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the organizer entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class OrganizerAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {

    $entity_type_id = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $is_owner = $entity->getOwnerId() === $account->id();
    $entity_owner = FALSE;
    $user = \Drupal\user\Entity\User::load($account->id());

    switch ($operation) {
      case 'view':   

        if ($is_owner) {
          return AccessResult::allowedIfHasPermissions($account, ["view own organizer $bundle content", 'view own organizer', 'administer organizer types' , 'administer organizer'], 'OR');
        }
        
        return AccessResult::allowedIfHasPermissions($account, ["view any organizer $bundle content", 'view any organizer', 'administer organizer types', 'administer organizer'], 'OR');

      case 'update':
       
        if ($is_owner) {
          return AccessResult::allowedIfHasPermissions($account, ["edit own organizer $bundle content", 'edit own organizer', 'administer organizer types', 'administer organizer'], 'OR');
        }
        
        return AccessResult::allowedIfHasPermissions($account, ["edit any organizer $bundle content", 'edit any organizer', 'administer organizer', 'administer organizer types'], 'OR');
      
      case 'delete':

        if ($is_owner) {
          return AccessResult::allowedIfHasPermissions($account, ["delete own organizer $bundle content", 'delete own organizer', 'administer organizer', 'administer organizer types'], 'OR');
        }

        return AccessResult::allowedIfHasPermissions($account, ["delete any organizer $bundle content", 'delete any organizer', 'administer organizer', 'administer organizer types'], 'OR');
    
    }    

    // Unknown operation, no opinion.
    return AccessResult::neutral();

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ["create {$context['entity_type_id']} $entity_bundle content", 'administer organizer types'], 'OR');
  } 


}
