<?php declare(strict_types = 1);

namespace Drupal\organizer;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of organizer type entities.
 *
 * @see \Drupal\organizer\Entity\OrganizerType
 */
final class OrganizerTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No organizer types available. <a href=":link">Add organizer type</a>.',
      [':link' => Url::fromRoute('entity.organizer_type.add_form')->toString()],
    );

    return $build;
  }

}
