<?php

/**
 * @file
 * Organizer module query hooks.
 */

use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\organizer\Entity\Organizer;

/**
 * Implements hook_query_TAG_alter().
 *
 * This hook implementation adds organizer access checks for the
 * account stored in the 'account' meta-data (or current user if not provided),
 * for an operation stored in the 'op' meta-data (or 'view' if not provided).
 */
function organizer_query_organizer_access_alter(AlterableInterface $query) {
  /** @var \Drupal\Core\Database\Query\SelectInterface $query */
  $op = $query->getMetaData('op') ?: 'view';
  $account = $query->getMetaData('account') ?: \Drupal::currentUser();
  
  $entity_type = \Drupal::entityTypeManager()->getDefinition('organizer');

  // Get organizer tables which are used to build the alter query.
  $organizer_tables = [];
  foreach ($query->getTables() as $table) {
    if (is_string($table['table']) && $table['table'] === $entity_type->getBaseTable()) {
      $organizer_tables[] = [
        'alias' => $table['alias'],
        'condition' => $query->orConditionGroup(),
      ];
    }
  }

  // If there are no organizer tables then nothing needs to be altered.
  if (empty($organizer_tables)) {
    return;
  }

  // If the user has administer access then exit.
  if ($account->hasPermission('administer organizer types')
    || $account->hasPermission('administer organizer')) {
    return;
  }

  // Apply operation specific any and own permissions.
  if (in_array($op, ['view', 'edit', 'delete'])) {
    $permission_any = "$op any organizer";
    $permission_own = "$op own organizer";

    // If the user has any permission the query does not have to be altered.
    if ($account->hasPermission($permission_any)) {
      return;
    }

    // If the user has own permission, then add the account id to all
    // organizer tables conditions.
    if ($account->hasPermission($permission_own)) {
      foreach ($organizer_tables as $table) {
        $table['condition']->condition($table['alias'] . '.uid', $account->id());
      }
    }
  }



  // Allow modules to alter and add their query access rules.
  \Drupal::moduleHandler()->alter(
    'organizer_query_access',
    $query,
    $organizer_tables
  );

  // Apply webform submission table conditions to query.
  foreach ($organizer_tables as $table) {
    // If a webform submission table does not have any conditions,
    // we have to block access to the table.
    if (count($table['condition']->conditions()) === 1) {
      $table['condition']->where('1 = 0');
    }

    $query->condition($table['condition']);
  }
}

